function hamburgerbtn() {
    let image = document.querySelector(`.hamburger-btn`)
    if(image.alt == "hamburger"){
        image.alt = "close"
        image.src = "/images/icon-close.svg"
        document.querySelector(`.mobile-hamburger-btn`).style.display = "block"

    }
    else{
        image.alt = "hamburger"
        image.src = "/images/icon-hamburger.svg"
        document.querySelector(`.mobile-hamburger-btn`).style.display = "none"
    }

}